package com.capgem.hotelbooking.pageobjects;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends AbstractPage {

	Logger log = Logger.getLogger(HomePage.class.getName());

	@FindBy(id = "username")
	private WebElement userName;

	@FindBy(id = "password")
	private WebElement password;

	@FindBy(id = "doLogin")
	private WebElement btnLogin;

	// @FindBy(HOTEL_ROW_PATH = "//button[contains(text(),'Close')]")
	// private WebElement btn_LoginClose;

	@FindBy(linkText = "Home")
	private WebElement homeLink;

	@FindBy(linkText = "Login")
	private WebElement loginLink;

	@FindBy(linkText = "Logout")
	private WebElement logoutLink;

	@FindBy(id = "hotelName")
	private WebElement hotelName;

	@FindBy(id = "address")
	private WebElement address;

	@FindBy(id = "owner")
	private WebElement owner;

	@FindBy(id = "phone")
	private WebElement phone;

	@FindBy(id = "email")
	private WebElement email;

	@FindBy(id = "createHotel")
	private WebElement createHotel;

	private static final By ROW_PATH = By.xpath("//div[contains(@class,'row detail')]");
	private static final By HOTEL_PATH = By.xpath("./div/div/p");
	private static final By DELETE_PATH = By.className("hotelDelete");
	private static final By HOTEL_ROW_PATH = By.xpath("//div[contains(@class,'hotelRow')]");
	private final WebDriverWait wait;

	private WebDriver driver;

	public HomePage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, 5);
	}

	public void clickLink(String linkText) {
		switch (linkText.toLowerCase()) {
		case "home":
			homeLink.click();
			break;
		case "login":
			loginLink.click();
			break;

		case "logout":
			logoutLink.click();
			break;

		default:
			log.warning("Link not found");
			break;
		}
	}

	public boolean verifyLogin() {
		return true;

	}

	public void inputUserCredentials(String user, String pwd) {
		log.info("User Crentential entered");
		driver.switchTo().activeElement();
		userName.sendKeys(user);
		password.sendKeys(pwd);
	}

	public void clickLoginButton() {
		btnLogin.click();
	}

	// public void clickLoginCloseButton() {
	// }

	public boolean isLoginSuccessful() throws Exception {
		log.info("verify login success");
		waitForElementDisplayed(logoutLink, 5);
		return logoutLink.isDisplayed();
	}

	/**
	 * Fills the hotel entries based the given tableData param
	 * 
	 * @param tableData
	 * @throws Exception
	 */
	public void inputHotelEntries(Map<String, String> tableData) throws Exception {
		if (tableData == null || tableData.isEmpty()) {
			throw new IllegalArgumentException("No Hotel Entries Found");
		}
		getWaitedElement(hotelName).sendKeys(tableData.get("hotelName"));
		getWaitedElement(address).sendKeys(tableData.get("address"));
		getWaitedElement(owner).sendKeys(tableData.get("owner"));
		getWaitedElement(phone).sendKeys(tableData.get("phone"));
		getWaitedElement(email).sendKeys(tableData.get("email"));
		clickCreateButton();
		log.info("filled hotel entries");
	}

	public void clickCreateButton() {
		getWaitedElement(createHotel).click();
	}

	/**
	 * Returns all the hotel entries available in the page
	 * 
	 * @param totalEntries
	 *            required wait for all elements to present based on the given
	 *            test data
	 * @param reqEntry
	 * @return
	 * @throws Exception
	 */
	public String[] getHotelEntry(int totalEntries, int reqEntry) throws Exception {
		waitUntilCountChanges(HOTEL_ROW_PATH, totalEntries, 5);
		List<WebElement> findElements = driver.findElements(HOTEL_ROW_PATH);
		WebElement webElement = findElements.get(reqEntry);
		String text = webElement.getText();
		if (text != null) {
			return text.split("\n");
		}
		return new String[] {};
	}

	/**
	 * Delete the Hotel entry with the given parameter
	 * 
	 * @param hotelName
	 * @throws InterruptedException
	 */
	public void deleteEntryByName(String hotelName) {
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(ROW_PATH));
		List<WebElement> we = driver.findElements(ROW_PATH);
		int i = 0;
		for (WebElement webElement : we) {
			wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(HOTEL_PATH));
			String text = webElement.findElement(HOTEL_PATH).getText();
			if (text.equals(hotelName)) {
				break;
			}
			i++;
		}

		List<WebElement> element = driver.findElements(DELETE_PATH);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element.get(i));

	}

	/**
	 * Check whether the given hotel name is present in the page
	 * 
	 * @param hotelName
	 * @return
	 * @throws InterruptedException
	 */
	public boolean isHotelPresent(String hotelName) {
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(ROW_PATH));
		List<WebElement> we = driver.findElements(ROW_PATH);
		for (WebElement webElement : we) {
			String text = webElement.findElement(HOTEL_PATH).getText();
			if (text.equals(hotelName)) {
				return true;
			}
		}
		return false;

	}

}
