package com.capgem.hotelbooking.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AbstractPage {

    private WebDriver driver;

    public AbstractPage(WebDriver driver) {
	this.driver = driver;
	PageFactory.initElements(driver, this);
    }

    public void waitForElementDisplayed(WebElement element, int seconds) throws Exception {
	WebDriverWait wait = new WebDriverWait(driver, seconds);
	wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void waitUntilCountChanges(By by, int elementsSize, int waitInSec) {
	WebDriverWait wait = new WebDriverWait(driver, waitInSec);
	wait.until(ExpectedConditions.numberOfElementsToBe(by, elementsSize));
    }

    public WebElement getWaitedElement(WebElement element) {
	return new WebDriverWait(driver, 3).until(ExpectedConditions.visibilityOf(element));
    }

}
