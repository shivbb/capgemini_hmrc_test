Feature: Create and Delete entries

  Scenario: check login success
    Given user navigates to home page
    When user clicks on "Login" link
    And user login with username as "admin" and password as "password"
    Then verify login successful

  Scenario: Create an entry
    Given user login into application with username as "admin" and password as "password"
    When user inputs following hotel details
      | hotelName    | address    | owner | phone       | email            |
      | Travel Lodge | Address -1 | alex  | 08719846409 | booking@tl.co.uk |
    Then create entry should be successful

  Scenario: Create multiple entries
    Given user login into application with username as "admin" and password as "password"
    When user inputs following hotel details
      | a1 | a1-addr | a1-owner | a1-phone | a1@a1.com |
      | a2 | a2-addr | a2-owner | a2-phone | a2@a2.com |
      | a3 | a3-addr | a3-owner | a3-phone | a3@a3.com |
    Then create entries should be successful

  Scenario: Delete an entry
    Given user login into application with username as "admin" and password as "password"
    When user clicks on "Delete" button for the Hotel Name as "Travel Lodge2"
    Then corresponding entry should be deleted
