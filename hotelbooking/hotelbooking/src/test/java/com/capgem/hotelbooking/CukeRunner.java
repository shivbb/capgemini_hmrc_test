package com.capgem.hotelbooking;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "json:target/capgem_test_report.json",
	"html:target/booking_test_report.html" }, features = "src/test/resources/features", glue = {
		"classpath:com.capgem.hotelbooking.steps" })
public class CukeRunner {
}
