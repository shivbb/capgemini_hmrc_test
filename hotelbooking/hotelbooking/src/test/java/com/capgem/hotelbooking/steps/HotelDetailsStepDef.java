package com.capgem.hotelbooking.steps;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.Assert;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.capgem.hotelbooking.pageobjects.HomePage;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class HotelDetailsStepDef {

    private Logger log = Logger.getLogger(HotelDetailsStepDef.class.getName());
    private WebDriver driver;
    private HomePage homePage;
    private List<Map<String, String>> inputHotelDetailsMap;

    /**
     * Delete all cookies at the start of each scenario to avoid shared state
     * between tests
     */
    @Before
    public void startUp() throws MalformedURLException {
	System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
	driver = new ChromeDriver();
	driver.manage().deleteAllCookies();
	ChromeOptions options = new ChromeOptions();
	options.addArguments("--start-maximized");
	options.addArguments("--disable-extensions");
	driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	driver.manage().window().setSize(new Dimension(1024, 768));
	driver.manage().window().maximize();
	driver.get("http://localhost:3003/");
	if (homePage == null) {
	    homePage = new HomePage(driver);
	}
    }

    @After
    public void tearDown(Scenario scenario) {
	if (scenario.isFailed()) {
	    try {
		scenario.write("Current Page URL is " + driver.getCurrentUrl());
		// byte[] screenshot = getScreenshotAs(OutputType.BYTES);
		byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
		scenario.embed(screenshot, "image/png");
	    } catch (WebDriverException ex) {
		log.severe(ex.getMessage());
	    }
	}
	driver.quit();
    }

    @Given("^user navigates to home page$")
    public void user_navigates_to_home_page() throws Throwable {
	homePage.clickLink("Home");
    }

    @And("^user login with username as \"([^\"]*)\" and password as \"([^\"]*)\"$")
    public void user_login_with_username_as_something_and_password_as_something(String user, String pwd)
	    throws Throwable {
	homePage.inputUserCredentials(user, pwd);
	homePage.clickLoginButton();
    }

    @Given("^user inputs following hotel details$")
    public void user_inputs_following_hotel_details(DataTable table) throws Throwable {
	inputHotelDetailsMap = table.asMaps(String.class, String.class);
	for (Map<String, String> map : inputHotelDetailsMap) {
	    homePage.inputHotelEntries(map);
	    Thread.sleep(1000);
	}

    }

    @And("^user clicks on Create button$")
    public void user_clicks_on_create_button() throws Throwable {
	homePage.clickCreateButton();
    }

    @Given("^user login into application with username as \"([^\"]*)\" and password as \"([^\"]*)\"$")
    public void user_login_into_application_with_username_as_something_and_password_as_something(String user,
	    String pwd) throws Throwable {
	homePage.clickLink("login");
	homePage.inputUserCredentials(user, pwd);
	homePage.clickLoginButton();
    }

    @When("^user clicks on \"([^\"]*)\" link$")
    public void user_clicks_on_a_link(String link) throws Throwable {
	homePage.clickLink(link);
    }

    @When("^user clicks on \"([^\"]*)\" button for the Hotel Name as \"([^\"]*)\"$")
    public void user_clicks_on_something_button_for_the_hotel_name_as_something(String strArg1, String strArg2)
	    throws Throwable {
	homePage.deleteEntryByName(strArg2);
//	homePage.deleteHotelEntry(0);
    }

    @Then("^verify login successful$")
    public void verify_login_successful() throws Throwable {
	Assert.assertTrue(homePage.isLoginSuccessful());
    }

    @Then("^create entry should be successful$")
    public void create_entry_should_be_successful() throws Throwable {
	String[] entry = homePage.getHotelEntry(inputHotelDetailsMap.size(), 0);
	Map<String, String> map = inputHotelDetailsMap.get(0);
	Object[] array = map.values().toArray();
	Assert.assertArrayEquals(entry, array);

    }

    @Then("^create entries should be successful$")
    public void create_entries_should_be_successful() throws Throwable {
	int i = 0;
	int totlaEntries = inputHotelDetailsMap.size();
	for (Map<String, String> map : inputHotelDetailsMap) {
	    String[] entry = homePage.getHotelEntry(totlaEntries, i++);
	    Object[] array = map.values().toArray();
	    Assert.assertArrayEquals(entry, array);
	}

    }

    @Then("^corresponding entry should be deleted$")
    public void corresponding_entry_should_be_deleted() throws Throwable {
	homePage.isHotelPresent("a1");
    }

}